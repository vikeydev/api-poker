'use strict';
var mongoose = require('mongoose'),
Evento = mongoose.model('Evento'),
Investimento = mongoose.model('Investimento'),
Jogador = mongoose.model('Jogador'),
Participacao = mongoose.model('Participacao');
exports.listar = function(req, res) {
    Participacao.find(req.query, {}, { sort: { 'saldo': -1, 'faturamento': -1, 'investimento': -1, 'dataConfirmacao': 1 } },
        function(err, participacoes) {
            if (err) {
                res.send(err);
            } else {
                var len = participacoes.length;
                var count = 0;

                if(len <= 0) {
                    res.json([]);
                } else {
                    for(var i in participacoes) {
                        ( function(curIdx) {
                            Jogador.findById(participacoes[curIdx].idJogador, function(err, jogador) {
                                if (err) {
                                    res.send(err);
                                } else {
                                    participacoes[curIdx].set('jogador', jogador, { strict: false });
                                    /*Evento.findById(participacoes[curIdx].idEvento, function(err, evento) {
                                        if (err) {
                                            res.send(err);
                                        } else {
                                            participacoes[curIdx].set('evento', evento, { strict: false });*/
                                            if (++count == len) {
                                                res.json(participacoes);
                                            }
                                        //}
                                    //})
                                }
                            });
                        })(i);
                    }
                }
            }
        }
    );
};

exports.criar = function(req, res) {
    var newParticipacao = new Participacao(req.body);
    newParticipacao.save(function(err, participacao) {
        if (err) {
            res.send(err);
        } else {
            Evento.findById(newParticipacao.idEvento, function(err, evento) {
                if (err) {
                    res.send(err);
                } else {
                    evento.qtdParticipante += 1;
                    Evento.findOneAndUpdate({ _id: newParticipacao.idEvento }, evento, { new: true }, function(err, newEvento) {
                        if (err) {
                            res.send(err);
                        } else {
                            res.json(participacao);
                        }
                    });
                }
            });
        }
    });
};

exports.ler = function(req, res) {
    Participacao.findById(req.params.participacaoId, function(err, participacao) {
        if (err) {
            res.send(err);
        } else {
            res.json(participacao);
        }
    });
};

exports.editar = function(req, res) {
    Participacao.findOneAndUpdate({_id: req.params.participacaoId}, req.body, {new: true}, function(err, participacao) {
        if (err) {
            res.send(err);
        } else {
            res.json(participacao);
        }
    });
};

exports.excluir = function(req, res) {
    Participacao.findById(req.params.participacaoId, function(err, participacao) {
        if (err) {
            res.send(err);
        } else {
            if(participacao == null) {
                res.json({ mensagem: 'O jogador não está confirmado no evento.' });
            } else {
                var queryInvestimento = { idJogador: participacao.idJogador, idEvento: participacao.idEvento };
                //console.log(queryInvestimento);
                Investimento.find(queryInvestimento, function(err, investimentos) {
                    if (err) {
                        res.send(err);
                    } else {
                        if(investimentos.length > 0) {
                            res.json({ mensagem: 'O jogador possui investimentos no evento.' });
                        } else {
                            Participacao.deleteOne({ _id: req.params.participacaoId }, function(err, del) {
                                if (err) {
                                    res.send(err);
                                } else {
                                    Evento.findById(participacao.idEvento, function(err, evento) {
                                        if (err) {
                                            res.send(err);
                                        } else {
                                            if(evento.qtdParticipante > 0) {
                                                evento.qtdParticipante -= 1;
                                            }
                                            Evento.findOneAndUpdate({ _id: participacao.idEvento }, evento, { new: true }, function(err, newEvento) {
                                                if (err) {
                                                    res.send(err);
                                                } else {
                                                    res.json({ mensagem: 'O jogador não participa mais do evento.' });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            }
        }
    });
};

exports.finalizar = function(req, res) {
    var listaParticipacao = req.body.participacoes;
    var idEvento = req.body.idEvento;
    var totalFaturamento = 0;
    var naoRetornou = true; // GAMBIARRA - resolver
    // Soma faturamento do request
    listaParticipacao.forEach(function(part) { totalFaturamento += part.faturamento; });
    // Soma investimentos do evento
    Investimento.aggregate([{ $match: { idEvento: idEvento } },
                                      { $group: { _id: idEvento , total: { $sum: "$valor" } } },
                                      { $sort: { total: -1 } }], 
        function(err, sum) {
            var totalInvestimento = sum[0].total;

            if(totalFaturamento > totalInvestimento) {
                var saldoMais = totalFaturamento - totalInvestimento;
                res.json({ mensagem: 'A soma dos valores informados = ' + totalFaturamento + ' é superior a soma de investimentos = ' + totalInvestimento + ". Sobra " + saldoMais + "."});
            } else if(totalFaturamento < totalInvestimento) {
                var saldoMenos = totalInvestimento - totalFaturamento;
                res.json({ mensagem: 'A soma dos valores informados = ' + totalFaturamento + ' é inferior a soma de investimentos = ' + totalInvestimento + ". Falta " + saldoMenos + "." });
            } else {
                var lenParticipacao = listaParticipacao.length;
                var idxParticipacao = 0;
                // percorrer participacoes
                listaParticipacao.forEach(function(new_participacao) {
                    var query = { idJogador: new_participacao.idJogador, idEvento: idEvento };

                    // buscar confirmacao do evento
                    Participacao.find(query, function(err, objParticipacao) {
                        
                        // consultar investimentos do jogador no evento
                        Investimento.find(query, {}, { sort: { 'dataCriacao': 1 } },
                            function(err, investimentos) {
                                if (err) {
                                    res.send(err);
                                } else {
                                    var lenInvestimento = investimentos.length;
                                    var qtdInvestimento = 0;
                                    var qtdReBuy = 0;
                                    var qtdAddOn = 0;
                                    var vlrInvestimento = 0;

                                    // percorrer investimentos do jogador do evento
                                    investimentos.forEach(function(investimento) {
                                        qtdInvestimento++;
                                        vlrInvestimento += investimento.valor;
                                        if(investimento.tipo == 'ReBuy') {
                                            qtdReBuy++;
                                        } else if(investimento.tipo == 'AddOn') {
                                            qtdAddOn++;
                                        }
                                        
                                        if (qtdInvestimento == lenInvestimento) { // fim dos investimentos do jogador do evento
                                            // calcula performance
                                            var saldo = new_participacao.faturamento - vlrInvestimento;
                                            var roi = saldo / vlrInvestimento * 100;
                                            // add valores na partipacao
                                            var update = {  
                                                            'qtdBuyIn': qtdInvestimento - qtdReBuy, 
                                                            'qtdReBuy': qtdReBuy, 
                                                            'qtdAddOn': qtdAddOn,
                                                            'investimento': vlrInvestimento,
                                                            'faturamento': new_participacao.faturamento,
                                                            'saldo': saldo, 
                                                            'roi': roi
                                                        };

                                            // salvar finalizacao de participacao
                                            idxParticipacao++;
                                            Participacao.findOneAndUpdate({_id: objParticipacao[0]._id}, update, { new: true }, function(err, upd) {
                                                if (err) {
                                                    res.send(err);
                                                } else {
                                                    if(lenParticipacao == idxParticipacao) {
                                                        if(naoRetornou) {
                                                            res.json({ mensagen: 'Participações finalizadas com sucesso' });
                                                            naoRetornou = false;
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        );
                    });
                });
            }
        }
    );
};