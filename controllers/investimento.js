'use strict';
var mongoose = require('mongoose'),
Evento = mongoose.model('Evento'),
Investimento = mongoose.model('Investimento'),
Jogador = mongoose.model('Jogador'),
Participacao = mongoose.model('Participacao');
exports.listar = function(req, res) {
    Investimento.find(req.query, {}, { sort: { 'dataCriacao': 1 } },
        function(err, investimentos) {
            if (err) {
                res.send(err);
            } else {
                var len = investimentos.length;
                var count = 0;

                if(len <= 0) {
                    res.json([]);
                } else {
                    for(var i in investimentos) {
                        ( function(curIdx) {
                            Jogador.findById(investimentos[curIdx].idJogador, function(err, jogador) {
                                if (err) {
                                    res.send(err);
                                } else {
                                    investimentos[curIdx].set('jogador', jogador, { strict: false });
                                    /*Evento.findById(investimentos[curIdx].idEvento, function(err, evento) {
                                        if (err) {
                                            res.send(err);
                                        } else {
                                            investimentos[curIdx].set('evento', evento, { strict: false });*/
                                            
                                            if (++count == len) {
                                                res.json(investimentos);
                                            }
                                        //}
                                    //});
                                }
                            });
                        })(i);
                    }
                }
            }
        }
    );
};
exports.ler = function(req, res) {
    Investimento.findById(req.params.investimentoId, function(err, investimento) {
        if (err) {
            res.send(err);
        } else {
            Jogador.findById(investimento.idJogador, function(err, jogador) {
                if (err) {
                    res.send(err);
                } else {
                    investimento.set('jogador', jogador, { strict: false });
                    Evento.findById(investimento.idEvento, function(err, evento) {
                        if (err) {
                            res.send(err);
                        } else {
                            investimento.set('evento', evento, { strict: false });
                            res.json(investimento);
                        }
                    })       
                }
            })
        }
    });
};
exports.editar = function(req, res) {
    Investimento.findOneAndUpdate({_id: req.params.investimentoId}, req.body, {new: true}, function(err, investimento) {
        if (err)
            res.send(err);
        res.json(investimento);
    });
};
exports.excluir = function(req, res) {
    Investimento.findById(req.params.investimentoId, function(err, investimento) {
        if (err) {
            res.send(err);
        } else {
            if(investimento == null) {
                res.json({ mensagem: 'Investimento inexistente' });
            } else {
                deleteInvestimentoDB(res, investimento);
            }
        }
    });
};

exports.excluirUltimoInvestimentoJogadorEvento = function(req, res) {
    Investimento.findOne({ 'idJogador': req.body.idJogador, 'idEvento': req.body.idEvento }, {}, { sort: { 'dataCriacao': -1 } }, function(err, investimento) {
        if(err) {
            res.send(err);
        } else {
            if(investimento != null) {
                deleteInvestimentoDB(res, investimento);
            } else {
                res.json({ mensagem: 'Os investimentos deste jogador neste evento já foram excluídos' });
            }
        }
    });
};

exports.criar = function(req, res) {
    var new_investimento = new Investimento(req.body);
    if(new_investimento.tipo == null && new_investimento.valor == null) {
        var queryInvestimento = { idEvento: new_investimento.idEvento, idJogador: new_investimento.idJogador, tipo: 'BuyIn' };
        Investimento.find(queryInvestimento, function(err, investimentos) {
            if (err) {
                res.send(err);
            } else {
                var tipoInvestimento = investimentos.length <= 0 ? 'BuyIn' : 'ReBuy';
                new_investimento.set('tipo', tipoInvestimento, { strict: false });
                Evento.findById(new_investimento.idEvento, function(err, evento) {
                    if (err) {
                        res.send(err);
                    } else {
                        if(evento.formato != 'CashGame') {
                            res.json({ mensagem: 'O sistema não está pronto para esse tipo de evento' });
                        } else {
                            var valorInvestimento = new_investimento.tipo == 'BuyIn' ? evento.buyIn : evento.reBuy;
                            new_investimento.set('valor', valorInvestimento, { strict: false });
                            new_investimento.save(function(err, investimento) {
                                if (err) {
                                    res.send(err);
                                } else {
                                    var qtdBuyInEvento = new_investimento.tipo == 'BuyIn' ? evento.qtdBuyIn+1 : evento.qtdBuyIn;
                                    var qtdReBuyEvento = new_investimento.tipo == 'ReBuy' ? evento.qtdReBuy+1 : evento.qtdReBuy;
                                    var dataInicioEvento = evento.dataInicio == null ? new Date() : evento.dataInicio;
                                    var statusEvento = evento.status == 'Programado' ? 'Em andamento' : evento.status;
                                    var updEvento = { 
                                        'investimento': evento.investimento + valorInvestimento, 
                                        'qtdBuyIn': qtdBuyInEvento,
                                        'qtdReBuy': qtdReBuyEvento,
                                        'dataInicio': dataInicioEvento,
                                        'status' : statusEvento
                                    };
                                    Evento.findOneAndUpdate({_id: evento._id}, updEvento, {new: true}, function(err, evento) {
                                        if (err) {
                                            res.send(err);
                                        } else {
                                            // consultar participacao
                                            var queryParticipacao = { idEvento: new_investimento.idEvento, idJogador: new_investimento.idJogador };
                                            Participacao.findOne(queryParticipacao, function(err, participacao) {
                                                if(err) {
                                                    res.send(err);
                                                } else {
                                                    var qtdBuyInParticipacao = new_investimento.tipo == 'BuyIn' ? participacao.qtdBuyIn+1 : participacao.qtdBuyIn;
                                                    var qtdReBuyParticipacao = new_investimento.tipo == 'ReBuy' ? participacao.qtdReBuy+1 : participacao.qtdReBuy;
                                                    var updParticipacao = { 
                                                        'investimento': participacao.investimento + valorInvestimento, 
                                                        'qtdBuyIn': qtdBuyInParticipacao,
                                                        'qtdReBuy': qtdReBuyParticipacao
                                                    };
                                                    Participacao.findOneAndUpdate({_id: participacao._id}, updParticipacao, { new: true }, function(err, part) {
                                                        if (err) {
                                                            res.send(err);
                                                        } else {
                                                            res.json(investimento);
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    }
                })
            }
        });
    } else {
        //throw ({ message: "O sistema não está pronto para este tipo de investimento" });
        res.json({ mensagem: "O sistema não está pronto para este tipo de investimento" });
    }
};

var deleteInvestimentoDB = function(res, investimento) {
    Investimento.deleteOne({ _id: investimento._id }, function(err, response) {
        if(err) {
            res.send(err);
        } else {
            Evento.findById(investimento.idEvento, function(err, evento) {
                if(err) {
                    res.send(err);
                } else {
                    var qtdBuyInEvento = 0;
                    if(evento.qtdBuyIn > 0) {
                        if(investimento.tipo == 'BuyIn') {
                            qtdBuyInEvento = evento.qtdBuyIn-1
                        } else {
                            qtdBuyInEvento = evento.qtdBuyIn;
                        }
                    }
                    var qtdReBuyEvento = 0;
                    if(evento.qtdReBuy > 0) {
                        if(investimento.tipo == 'ReBuy') {
                            qtdReBuyEvento = evento.qtdReBuy-1;
                        } else {
                            qtdReBuyEvento = evento.qtdReBuy;
                        }
                    }
                    var vlrInvestimentoEvento = evento.investimento > 0 ? evento.investimento - investimento.valor : 0;
                    var updEvento = { 
                        'investimento': vlrInvestimentoEvento, 
                        'qtdBuyIn': qtdBuyInEvento,
                        'qtdReBuy': qtdReBuyEvento
                    };
                    Evento.findOneAndUpdate({_id: evento._id}, updEvento, {new: true}, function(err, evento) {
                        if(err) {
                            res.send(err);
                        } else {
                            var queryParticipacao = { idJogador: investimento.idJogador, idEvento: investimento.idEvento };
                            Participacao.findOne(queryParticipacao, function(err, participacao) {
                                if(err) {
                                    res.send(err);
                                } else {
                                    var qtdBuyInParticipacao = 0;
                                    if(participacao.qtdBuyIn > 0) {
                                        if(investimento.tipo == 'BuyIn') {
                                            qtdBuyInParticipacao = participacao.qtdBuyIn-1
                                        } else {
                                            qtdBuyInParticipacao = participacao.qtdBuyIn;
                                        }
                                    }
                                    var qtdReBuyParticipacao = 0;
                                    if(participacao.qtdReBuy > 0) {
                                        if(investimento.tipo == 'ReBuy') {
                                            qtdReBuyParticipacao = participacao.qtdReBuy-1;
                                        } else {
                                            qtdReBuyParticipacao = participacao.qtdReBuy;
                                        }
                                    }
                                    var vlrInvestimentoParticipacao = participacao.investimento > 0 ? participacao.investimento - investimento.valor : 0;
                                    var updParticipacao = { 
                                        'investimento': vlrInvestimentoParticipacao, 
                                        'qtdBuyIn': qtdBuyInParticipacao,
                                        'qtdReBuy': qtdReBuyParticipacao
                                    };
                                    Participacao.findOneAndUpdate({ _id: participacao._id }, updParticipacao, {new: true}, function(err, evento) {
                                        if(err) {
                                            res.send(err);
                                        } else {
                                            res.json({ mensagem: 'Investimento excluído com sucesso' });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};