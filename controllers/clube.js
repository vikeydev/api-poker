'use strict';
var mongoose = require('mongoose'),
Clube = mongoose.model('Clube'),
Integrante = mongoose.model('Integrante'),
Jogador = mongoose.model('Jogador');
exports.listar = function(req, res) {
    Clube.find(req.query, function(err, clube) {
        if (err) {
            res.send(err);
        } else {
            res.json(clube);
        }
    });
};
exports.criar = function(req, res) {
    var new_clube = new Clube(req.body);
    new_clube.save(function(err, clube) {
        if (err) {
            res.send(err);
        } else {
            res.json(clube);
        }
    });
};
exports.ler = function(req, res) {
    Clube.findById(req.params.clubeId, function(err, clube) {
        if (err) {
            res.send(err);
        } else {
            Integrante.find({ idClube: clube._id }, function(err, integrantes) {
                if (err) {
                    res.send(err);
                } else {
                    var idJogadores = integrantes.map(function(integrante) { return integrante.idJogador; });
                    Jogador.find({ _id: { $in: idJogadores } }, {}, { sort: { 'nome': 1 } }, function(err, jogadores) {
                        if (err) {
                            res.send(err);
                        } else {
                            clube.set('jogadores', jogadores, { strict: false });
                            res.json(clube);
                        }
                    });
                }
            });
        }
    });
};
exports.editar = function(req, res) {
    Clube.findOneAndUpdate({_id: req.params.clubeId}, req.body, {new: true}, function(err, clube) {
        if (err) {
            res.send(err);
        } else {
            res.json(clube);
        }
    });
};
exports.excluir = function(req, res) {
    Clube.deleteOne({ _id: req.params.clubeId }, function(err, clube) {
        if (err) {
            res.send(err);
        } else {
            res.json({ mensagem: 'Clube excluído com sucesso' });
        }
    });
};