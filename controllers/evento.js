'use strict';
var mongoose = require('mongoose'),
Clube = mongoose.model('Clube'),
Campeonato = mongoose.model('Campeonato'),
CampeonatoEvento = mongoose.model('CampeonatoEvento'),
Endereco = mongoose.model('Endereco'),
Evento = mongoose.model('Evento'),
Jogador = mongoose.model('Jogador'),
Participacao = mongoose.model('Participacao'),
Ranking = mongoose.model('Ranking');
exports.listar = function(req, res) {
    Evento.find(req.query, function(err, eventos) {
        if (err) {
            res.send(err);
        } else {
            res.json(eventos);
        }
    });
};
exports.criar = function(req, res) {
    var idsCampeonatos = req.body.idsCampeonatos;
    delete req.body.idsCampeonatos;
    var new_evento = new Evento(req.body);
    new_evento.save(function(err, evento) {
        if (err) {
            res.send(err);
        } else {
            vincularCampeonatosAoEvento(idsCampeonatos, evento._id);
            res.json(evento);
        }
    });
};
exports.ler = function(req, res) {
    Evento.findById(req.params.eventoId, function(err, evento) {
        if (err) {
            res.send(err);
        } else {
            Clube.findById(evento.idClube, function(err, clube) {
                if (err) {
                    res.send(err);
                } else {
                    evento.set('clube', clube, { strict: false });
                    Endereco.findById(evento.idEndereco, function(err, endereco) {
                        if (err) {
                            res.send(err);
                        } else {
                            evento.set('endereco', endereco, { strict: false });

                            // buscar campeonatos do evento
                            CampeonatoEvento.find({ 'idEvento': evento._id }, function(err, campeonatosEventos) {
                                if (err) {
                                    res.send(err);
                                } else {
                                    var lenCampeonatoEvento = campeonatosEventos.length;
                                    var idxCampeonatoEvento = 0;
                                    var campeonatosCompletos = [];

                                    if(lenCampeonatoEvento <= 0) {
                                        res.json(evento);
                                    } else {
                                        // percorrer campeonatos do evento
                                        campeonatosEventos.forEach(function(campeonatoEvento) {
                                            // buscar objCampeonato
                                            Campeonato.findById(campeonatoEvento.idCampeonato, function(err, campeonato) {
                                                if (err) {
                                                    res.send(err);
                                                } else {
                                                    // add objCampeonato to list
                                                    campeonatosCompletos.push(campeonato);
                                                
                                                    idxCampeonatoEvento++;
                                                    if(lenCampeonatoEvento == idxCampeonatoEvento) {
                                                        // set lista de campeonatos do evento
                                                        evento.set('campeonatos', campeonatosCompletos, { strict: false });
                                                        res.json(evento);
                                                    }
                                                }
                                            });
                                        });
                                    }
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};
exports.editar = function(req, res) {
    var idsCampeonatos = req.body.idsCampeonatos;
    delete req.body.idsCampeonatos;
    Evento.findOneAndUpdate({_id: req.params.eventoId}, req.body, {new: true}, function(err, evento) {
        if (err) {
            res.send(err);
        } else {
            vincularCampeonatosAoEvento(idsCampeonatos, req.params.eventoId);
            res.json(evento);
        }
    });
};
exports.excluir = function(req, res) {
    Evento.deleteOne({ _id: req.params.eventoId }, function(err, evento) {
        if (err) {
            res.send(err);
        } else {
            res.json({ mensagem: 'Evento excluído com sucesso' });
        }
    });
};
exports.finalizar = function(req, res) {
    Evento.findById(req.params.eventoId, function(err, evento) {
        if (err) {
            res.send(err);
        } else {
            // consultar clube
            Clube.findById({ _id: evento.idClube }, function(err, clube) {
                if (err) {
                    res.send(err);
                } else {
                    if(evento.status != "Encerrado") {
                        clube.qtdEvento += 1;
                    }

                    // buscar campeonatos do evento
                    var queryCampeonatosEvento = { 'idEvento': evento._id };
                    CampeonatoEvento.find(queryCampeonatosEvento, function(err, campeonatosEventos) {
                        if (err) {
                            res.send(err);
                        } else {
                            var lengthCampeonatosEventos = campeonatosEventos.length;
                            var campeonatosEventosCompletos = [];
                            var countCampeonatosEventos = 0;

                            if(lengthCampeonatosEventos <= 0) {
                                console.log("Revisar sessão de eventos sem campeonatos");
                                res.json({ mensagem: "Revisar sessão de eventos sem campeonatos" });
                            } else {
                                for(var i in campeonatosEventos) {
                                    ( function(idxCampeonatosEventos) {
                                        var campeonatoEvento = campeonatosEventos[idxCampeonatosEventos];
                                    
                                        // consultar Campeonato
                                        Campeonato.findById({ _id: campeonatoEvento.idCampeonato }, function(err, campeonato) {
                                            if (err) {
                                                res.send(err);
                                            } else {
                                                if(evento.status != "Encerrado") {
                                                    campeonato.qtdEvento += 1;
                                                }
                                                campeonatosEventosCompletos[idxCampeonatosEventos] = campeonato;

                                                if (++countCampeonatosEventos == lengthCampeonatosEventos) {
                                                    // fim da lista dos campeonatos eventos
                                                    
                                                    // buscar participacoes do evento
                                                    var queryParticipacao = { 'idEvento':  evento._id };
                                                    Participacao.find(queryParticipacao, {}, { sort: { 'dataConfirmacao': 0 } }, function(err, participacoes) {
                                                        if (err) {
                                                            res.send(err);
                                                        } else {
                                                            var qtdParticipante = participacoes.length;
                                                            var currentParticipacaoIdx = 0;
                                                            
                                                            // start valores evento
                                                            var investimento = 0;
                                                            var qtdReBuy = 0;
                                                            var qtdAddOn = 0;
                                                            
                                                            if(qtdParticipante <= 0) {
                                                                res.json({ mensagem: 'O Evento não possui participações' });
                                                            } else {
                                                                atualizarJogadoresSemParticipacao(evento, participacoes, clube, campeonatosEventosCompletos);
                                                                // percorrer participacoes do evento
                                                                participacoes.forEach(function(participacao) {
                                                                    // somar valores evento
                                                                    investimento += participacao.investimento;
                                                                    qtdReBuy += participacao.qtdReBuy;
                                                                    qtdAddOn += participacao.qtdAddOn;
                                                
                                                                    // set valores da participacao para merge de rankings
                                                                    var rankingParticipacao = {
                                                                        'idJogador': participacao.idJogador,
                                                                        'investimento': participacao.investimento,
                                                                        'faturamento': participacao.faturamento,
                                                                        'saldo': participacao.saldo,
                                                                        'roi': participacao.roi,
                                                                        'melhorFaturamento': participacao.faturamento,
                                                                        'qtdEvento': 1
                                                                    };
                                                
                                                                    // Buscar ranking clube
                                                                    var queryRankingJogadorClube = { 'idClube': evento.idClube, 'idJogador': participacao.idJogador }
                                                                    Ranking.findOne(queryRankingJogadorClube, function(err, rankingJogadorClube) {
                                                                        if (err) {
                                                                            res.send(err);
                                                                        } else {
                                                                            // merge valores do ranking do jogador do clube com participacao do jogador
                                                                            if(rankingJogadorClube != null) {
                                                                                queryRankingJogadorClube = { '_id': rankingJogadorClube._id };
                                                    
                                                                                // valida se o evento está encerrado para não contabilizar mais de uma vez a participação no ranking do clube
                                                                                if(evento.status != "Encerrado") {
                                                                                    rankingJogadorClube.investimento += rankingParticipacao.investimento;
                                                                                    rankingJogadorClube.faturamento += rankingParticipacao.faturamento;
                                                                                    rankingJogadorClube.saldo += rankingParticipacao.saldo;
                                                                                    rankingJogadorClube.roi = rankingJogadorClube.saldo / rankingJogadorClube.investimento * 100; 
                                                                                    if(rankingJogadorClube.melhorFaturamento < rankingParticipacao.melhorFaturamento) {
                                                                                        rankingJogadorClube.melhorFaturamento = rankingParticipacao.melhorFaturamento;
                                                                                    }
                                                                                    rankingJogadorClube.qtdEvento++;
                                                                                    rankingJogadorClube.frequencia = rankingJogadorClube.qtdEvento / clube.qtdEvento * 100;
                                                                                    rankingJogadorClube.posicaoAnterior = rankingJogadorClube.posicaoAtual;
                                                                                }
                                                                            } else {
                                                                                rankingJogadorClube = rankingParticipacao;
                                                                                rankingJogadorClube.frequencia = 1 / clube.qtdEvento * 100;
                                                                            }
                                                                            rankingJogadorClube.idClube = evento.idClube;

                                                                            if(clube.melhorFaturamento <= rankingParticipacao.faturamento) {
                                                                                clube.melhorFaturamento = rankingParticipacao.faturamento;
                                                                                clube.idJogadorMelhorFaturamento = participacao.idJogador;
                                                                            }
                                                                            // criar ou editar ranking do jogador do clube
                                                                            Ranking.updateOne(queryRankingJogadorClube, rankingJogadorClube, { upsert: true }, function(err, rkJogadorClube) {
                                                                                if (err) {
                                                                                    res.send(err);
                                                                                } else {
                                                                                    var lengthCampeonatosEventosCompletos = campeonatosEventosCompletos.length;
                                                                                    var countCampeonatosEventosCompletos = 0;
                                                    
                                                                                    for(var j in campeonatosEventosCompletos) {
                                                                                        ( function(currentCampeonatosEventosCompletosIdx) {
                                                                                            var campeonatoCompleto = campeonatosEventosCompletos[currentCampeonatosEventosCompletosIdx];
                                                                                            
                                                                                            // consultar ranking jogador do campeonato
                                                                                            var queryRankingJogadorCampeonato = { 'idCampeonato': campeonatoCompleto._id, 'idJogador': participacao.idJogador };
                                                                                            Ranking.findOne(queryRankingJogadorCampeonato, function(err, rankingJogadorCampeonato) {
                                                                                                if (err) {
                                                                                                    res.send(err);
                                                                                                } else {
                                                                                                    // merge valores do ranking do jogador do campeonato com participacao do jogador
                                                                                                    if(rankingJogadorCampeonato != null) {
                                                                                                        queryRankingJogadorCampeonato = { '_id': rankingJogadorCampeonato._id };
                                                        
                                                                                                        // valida se o evento está encerrado para não contabilizar mais de uma vez a participação no ranking do campeonato
                                                                                                        if(evento.status != "Encerrado") {
                                                                                                            rankingJogadorCampeonato.investimento += rankingParticipacao.investimento;
                                                                                                            rankingJogadorCampeonato.faturamento += rankingParticipacao.faturamento;
                                                                                                            rankingJogadorCampeonato.saldo += rankingParticipacao.saldo;
                                                                                                            rankingJogadorCampeonato.roi = rankingJogadorCampeonato.saldo / rankingJogadorCampeonato.investimento * 100;
                                                                                                            if(rankingJogadorCampeonato.melhorFaturamento < rankingParticipacao.melhorFaturamento) {
                                                                                                                rankingJogadorCampeonato.melhorFaturamento = rankingParticipacao.melhorFaturamento;
                                                                                                            }
                                                                                                            rankingJogadorCampeonato.qtdEvento++;
                                                                                                            rankingJogadorCampeonato.frequencia = rankingJogadorCampeonato.qtdEvento / campeonatoCompleto.qtdEvento * 100;
                                                                                                            rankingJogadorCampeonato.naDisputa = (campeonatoCompleto.frequenciaMinima > rankingJogadorCampeonato.frequencia) ? false : true;
                                                                                                            rankingJogadorCampeonato.posicaoAnterior = rankingJogadorCampeonato.posicaoAtual;
                                                                                                        }
                                                                                                    } else {
                                                                                                        delete rankingParticipacao.idClube;
                                                                                                        rankingJogadorCampeonato = rankingParticipacao;
                                                                                                        rankingJogadorCampeonato.frequencia = 1 / campeonatoCompleto.qtdEvento * 100;
                                                                                                        rankingJogadorCampeonato.naDisputa = (campeonatoCompleto.frequenciaMinima > rankingJogadorCampeonato.frequencia) ? false : true;
                                                                                                    }
                                                                                                    rankingJogadorCampeonato.idCampeonato = campeonatoCompleto._id;

                                                                                                    if(campeonatoCompleto.melhorFaturamento <= rankingParticipacao.faturamento) {
                                                                                                        campeonatoCompleto.melhorFaturamento = rankingParticipacao.faturamento;
                                                                                                        campeonatoCompleto.idJogadorMelhorFaturamento = participacao.idJogador;
                                                                                                    }
                                                                                                    
                                                                                                    // criar ou editar ranking do jogador do campeonato
                                                                                                    Ranking.updateOne(queryRankingJogadorCampeonato, rankingJogadorCampeonato, { upsert: true }, function(err, rkJogadorCampeonato) {
                                                                                                        if (err) {
                                                                                                            console.log(err);
                                                                                                            res.send(err);
                                                                                                        } else {
                                                                                                            // atualizar campeoantosEventos completos com info participacao
                                                                                                            campeonatosEventosCompletos[currentCampeonatosEventosCompletosIdx] = campeonatoCompleto;
                                                                                                            
                                                                                                            if (++countCampeonatosEventosCompletos == lengthCampeonatosEventosCompletos) {
                                                                                                                // console.log('Fim campeonatos do evento');
                                                                                                                
                                                                                                                currentParticipacaoIdx++;
                                                                                                                if (currentParticipacaoIdx == qtdParticipante) {
                                                                                                                    // console.log('Fim participacoes do evento');

                                                                                                                    // count evento clube
                                                                                                                    if(evento.status != "Encerrado") {
                                                                                                                        clube.investimento += investimento;        
                                                                                                                    }
                                                                                                                    Clube.findOneAndUpdate({ _id: evento.idClube }, clube, { new: true }, function(err, clube) {
                                                                                                                        if (err) {
                                                                                                                            res.send(err);
                                                                                                                        } else {
                                                                                                                            if(lengthCampeonatosEventos <= 0) {
                                                                                                                                console.log("Revisar sessão de eventos sem campeonatos");
                                                                                                                                res.json({ mensagem: "Revisar sessão de eventos sem campeonatos" });
                                                                                                                            } else {
                                                                                                                                var lengthCampeonatosAtualizados = campeonatosEventos.length;
                                                                                                                                var idxCampeonatoAtualizado = 0;
                                                                                                                            
                                                                                                                                campeonatosEventosCompletos.forEach(function(campeonatoCompleto2) {
                                                                                                                                    if(evento.status != "Encerrado") {
                                                                                                                                        campeonatoCompleto2.valorPremiacao += campeonatoCompleto2.custoPorEvento * qtdParticipante;
                                                                                                                                        campeonatoCompleto2.investimento += investimento;
                                                                                                                                    }
                                                                                                                                    Campeonato.findOneAndUpdate({ _id: campeonatoCompleto2._id }, campeonatoCompleto2, { new: true }, function(err, clube) {
                                                                                                                                        if (err) {
                                                                                                                                            res.send(err);
                                                                                                                                        } else {
                                                                                                                                            idxCampeonatoAtualizado++;
                                                                                                                                            if(lengthCampeonatosAtualizados == idxCampeonatoAtualizado) {
                                                                                                                                                // console.log('Fim da atualização de campeonatos do evento');

                                                                                                                                                // set valores evento
                                                                                                                                                evento.set('qtdParticipante', qtdParticipante, { strict: false });
                                                                                                                                                evento.set('qtdBuyIn', qtdParticipante, { strict: false });
                                                                                                                                                evento.set('investimento', investimento, { strict: false });
                                                                                                                                                evento.set('qtdReBuy', qtdReBuy, { strict: false });
                                                                                                                                                evento.set('qtdAddOn', qtdAddOn, { strict: false });
                                                                                                                                                evento.set('status', 'Encerrado', { strict: false });
                                                                                                                                                evento.set('dataFim', new Date(), { strict: false });
                                                                                                                                                
                                                                                                                                                Evento.findOneAndUpdate({ _id: req.params.eventoId }, evento, { new: true }, function(err, evento) {
                                                                                                                                                    if (err) {
                                                                                                                                                        res.send(err);
                                                                                                                                                    } else {
                                                                                                                                                        console.log("FINISH");
                                                                                                                                                        res.json(evento);
                                                                                                                                                    }
                                                                                                                                                });
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    });
                                                                                                                                });
                                                                                                                            }
                                                                                                                        }
                                                                                                                    });
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        })(j);
                                                                                    }
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                });
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    })(i);
                                }
                            }
                        }
                    });
                }
            });
        }
    });
};
const vincularCampeonatosAoEvento = function(idsCampeonatos, idEvento) {
    if(idsCampeonatos != null && idsCampeonatos.length > 0) {
        // percorrer campeonatos
        var len = idsCampeonatos.length;
        var idx = 0;
        idsCampeonatos.forEach(function(idCampeonato) {
            var newCampeonatoEvento = new CampeonatoEvento({ "idCampeonato": idCampeonato, "idEvento": idEvento });
            newCampeonatoEvento.save(function(err, campeonatoEvento) {
                if (err)
                    res.send(err);
                idx++;
                if(len == idx) {
                    return true;
                }
            });
        });
    } else {
        return true;
    }
};

const atualizarJogadoresSemParticipacao = function(evento, participacoes, clube, campeonatos) {
    var qtdParticipacao = participacoes.length;
    var contParticipacao = 0;
    var idsJogadores = [];
    participacoes.forEach(function(participacao) {
        idsJogadores.push(participacao.idJogador);
        if(++contParticipacao == qtdParticipacao) { // array lista id jogadores completa
            
            var queryRankingClube = { 'idClube': clube._id };
            Ranking.find(queryRankingClube, function(err, rankingsClube) {
                if(rankingsClube.length > 0) {
                    // percorre ranking dos jogadores do clube
                    rankingsClube.forEach(function(rankingJogadorClube) {
                        if(idsJogadores.indexOf(rankingJogadorClube.idJogador) > -1) {
                        } else {
                            var frequenciaAtualizada = rankingJogadorClube.qtdEvento / clube.qtdEvento * 100;
                            var posicaoAnteriorAtualizada = evento.status != 'Encerrado' ? rankingJogadorClube.posicaoAtual : rankingJogadorClube.posicaoAnterior;
                            Ranking.findOneAndUpdate({ _id: rankingJogadorClube._id }, { frequencia: frequenciaAtualizada, posicaoAnterior: posicaoAnteriorAtualizada }, { new: true }, function(err, a){
                                if(err) console.log(err);
                            });
                        }
                    });
                }
            });

            if(campeonatos.length > 0) {
                campeonatos.forEach(function(campeonato) {
                    var queryRankingCampeonato = { 'idCampeonato': campeonato._id };
                    console.log(queryRankingCampeonato);
                    Ranking.find(queryRankingCampeonato, function(err, rankingsCampeonato) {
                        if(rankingsCampeonato.length > 0) {
                            // percorre ranking dos jogadores do clube
                            rankingsCampeonato.forEach(function(rankingJogadorCampeonato) {
                                if(idsJogadores.indexOf(rankingJogadorCampeonato.idJogador) > -1) {
                                    //console.log("PARTICIPOU DO ULTIMO EVENTO.");
                                } else {
                                    //console.log("NÃO PARTICIPOU DO ULTIMO EVENTO.");
                                    var frequenciaAtualizada = rankingJogadorCampeonato.qtdEvento / campeonato.qtdEvento * 100;
                                    var disputaAtualizada = frequenciaAtualizada < campeonato.frequenciaMinima ? false : true;
                                    var posicaoAnteriorAtualizada = evento.status != 'Encerrado' ? rankingJogadorCampeonato.posicaoAtual : rankingJogadorCampeonato.posicaoAnterior;
                                    var updRankingJogadorCampeonato = { 
                                        frequencia: frequenciaAtualizada,
                                        naDisputa: disputaAtualizada,
                                        posicaoAnterior: posicaoAnteriorAtualizada
                                    };
                                    Ranking.findOneAndUpdate({ _id: rankingJogadorCampeonato._id }, updRankingJogadorCampeonato, { new: true }, function(err, b){
                                        console.log("TESTE: "+rankingJogadorCampeonato.idJogador);
                                        console.log("Eventos Jogador: " + rankingJogadorCampeonato.qtdEvento + " - Eventos Campeonato: " + campeonato.qtdEvento);
                                        if(err) console.log(err);
                                    });
                                }
                            });
                        }
                    });
                });
            }
        }
    });
};

exports.listarCampeonatos = function(req, res) {
    var query = { idEvento:  req.params.eventoId }
    CampeonatoEvento.find(query, function(err, campeonatos) {
        if (err) {
            res.send(err);
        } else {
            res.json(campeonatos);
        }
    });
}