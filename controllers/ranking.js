'use strict';
var mongoose = require('mongoose'),
Jogador = mongoose.model('Jogador'),
Ranking = mongoose.model('Ranking');
exports.listar = function(req, res) {
    if(req.query.idClube == null && req.query.idCampeonato == null) {
        res.json({ mensagem: 'É obrigatório filtrar por clube ou campeonato.' });
    } else {
        Ranking.find(req.query, {}, { sort: { 'saldo': -1, 'frequencia': -1, 'faturamento': -1, 'roi': -1, 'melhorFaturamento': -1, 'investimento': 1, 'qtdEvento': -1, 'idJogador': 1 } }, function(err, rankings) {
            if (err) {
                res.send(err);
            } else {
                var count = 0;
                if(rankings.length <= 0) {
                    res.json([]);
                } else {
                    for(var i in rankings) {
                        ( function(idx) {
                            Jogador.findById(rankings[idx].idJogador, function(err, jogador) {
                                if (err) {
                                    res.send(err);
                                } else {
                                    rankings[idx].set('jogador', jogador, { strict: false });

                                    var posicaoAtual = Number(idx)+1;
                                    rankings[idx].set('posicaoAtual', posicaoAtual, { strict: false });
                                    var posicaoVariacao = rankings[idx].posicaoAnterior == 0 ? 0 : rankings[idx].posicaoAnterior - posicaoAtual;
                                    rankings[idx].set('posicaoVariacao', posicaoVariacao, { strict: false });

                                    if (++count == rankings.length) {
                                        atualizarPosicaoRanking(rankings);
                                        res.json(rankings);
                                    }
                                }
                            });
                        })(i);
                    }
                }
            }
        });
    } 
};
exports.criar = function(req, res) {
    var new_ranking = new Ranking(req.body);
    new_ranking.save(function(err, ranking) {
        if (err) {
            res.send(err);
        } else {
            res.json(ranking);
        }
    });
};
exports.ler = function(req, res) {
    Ranking.findById(req.params.rankingId, function(err, ranking) {
        if (err) {
            res.send(err);
        } else {
            res.json(ranking);
        }
    });
};
exports.editar = function(req, res) {
    Ranking.findOneAndUpdate({_id: req.params.rankingId}, req.body, {new: true}, function(err, ranking) {
        if (err) {
            res.send(err);
        } else {
            res.json(ranking);
        }
    });
};
exports.excluir = function(req, res) {
    /*Ranking.find({}, function(err, rankings) {
        var len = rankings.length;
        var idx = 0;
        rankings.forEach(function(ranking) {
            Ranking.deleteOne({ _id: ranking._id }, function(err, ranking) {
                if (err) {
                    res.send(err);
                } else {
                    idx++;
                    if(len == idx) {
                        res.json({ mensagem: 'Os dados do Ranking foram desconsiderados.' });
                    }
                }
            });
        }); 
    });*/
    res.json({ mensagem: 'Não é possível excluir rankings.' });
};

const atualizarPosicaoRanking = function(rankings) {
    for(var i in rankings) {
        ( function(idx) {
            Ranking.findOneAndUpdate({ _id: rankings[idx]._id }, { posicaoAtual: rankings[idx].posicaoAtual }, { new: true }, function(err, ranking) {
                if (err) {
                    console.log(err);
                }
            });
        })(i);
    }
}