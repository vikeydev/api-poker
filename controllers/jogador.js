'use strict';
var mongoose = require('mongoose'),
Clube = mongoose.model('Clube'),
Integrante = mongoose.model('Integrante'),
Jogador = mongoose.model('Jogador');
exports.listar = function(req, res) {
    Jogador.find(req.query, {}, { sort: { 'nome': 1 } }, 
        function(err, jogadores) {
            if (err) {
                res.send(err);
            } else {
                res.json(jogadores);
            }
        }
    );
};
exports.criar = function(req, res) {
    var new_jog = new Jogador(req.body);
    new_jog.save(function(err, jogador) {
        if (err) {
            res.send(err);
        } else {
            res.json(jogador);
        }
    });
};
exports.ler = function(req, res) {
    Jogador.findById(req.params.jogadorId, function(err, jogador) {
        if (err) {
            res.send(err);
        } else {  
            Integrante.find({ idJogador: jogador._id }, function(err, integrantes) {
                if (err) {
                    res.send(err);
                } else {
                    var idClubes = integrantes.map(function(integrante) { return integrante.idClube; });
                    Clube.find({ _id: { $in: idClubes } }, function(err, clubes) {
                        if (err) {
                            res.send(err);
                        } else {
                            jogador.set("clubes", clubes, { strict: false });
                            res.json(jogador);
                        }
                    });
                }
            });
        }
    });
};
exports.editar = function(req, res) {
    Jogador.findOneAndUpdate({_id: req.params.jogadorId}, req.body, {new: true}, function(err, jogador) {
        if (err) {
            res.send(err);
        } else {
            res.json(jogador);
        }
    });
};
exports.excluir = function(req, res) {
    Jogador.deleteOne({ _id: req.params.jogadorId }, function(err, jogador) {
        if (err) {
            res.send(err);
        } else {
            res.json({ mensagem: 'Jogador foi excluído com sucesso' });
        }
   });
};