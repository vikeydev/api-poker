'use strict';
var mongoose = require('mongoose'),
Campeonato = mongoose.model('Campeonato'),
CampeonatoEvento = mongoose.model('CampeonatoEvento'),
Clube = mongoose.model('Clube'),
Evento = mongoose.model('Evento');
exports.listar = function(req, res) {
    Campeonato.find(req.query, function(err, campeonatos) {
        if (err) {
            res.send(err);
        } else {
            var lenC = campeonatos.length;
            var curIdxC = 0;
            var campeonatosClubeEventos = [];

            if(lenC <= 0) {
                res.json([]);
            } else {
                /*campeonatos.forEach(function(campeonato) {
                    // Buscar objClube
                    Clube.findById(campeonato.idClube, function(err, clube) {
                        if (err) {
                            res.send(err);
                        } else {
                            // set objClube
                            campeonato.set('clube', clube, { strict: false });

                            // Buscar Lista Ids Eventos
                            var queryCampeonato = { 'idCampeonato': campeonato._id };
                            CampeonatoEvento.find(queryCampeonato, function(err, idsCampeonatosEventos) {
                                if (err) {
                                    res.send(err);
                                } else {
                                    var lenCE = idsCampeonatosEventos.length;
                                    var curIdxCE = 0;
                                    var eventos = [];
                                    
                                    if(lenCE <= 0) {
                                        // add empty objList
                                        campeonato.set('eventos', [], { strict: false });
                                        campeonatosClubeEventos.push(campeonato);
                                        ++curIdxC;

                                        if (curIdxC == lenC) {
                                            res.json(campeonatosClubeEventos);
                                        }
                                    } else {
                                        idsCampeonatosEventos.forEach(function(campeonatoEvento) {
                                            // Buscar Evento
                                            Evento.findById(campeonatoEvento.idEvento, function(err, evento) {
                                                if (err) {
                                                    res.send(err);
                                                } else {
                                                    // add Evento to list
                                                    eventos.push(evento);
                                                    ++curIdxCE;

                                                    if (curIdxCE == lenCE) {
                                                        // set EventoList
                                                        campeonato.set('eventos', eventos, { strict: false });
                                                        // add Campeonato to fullObjList
                                                        campeonatosClubeEventos.push(campeonato);
                                                        ++curIdxC;

                                                        if (curIdxC == lenC) {
                                                            res.json(campeonatosClubeEventos);
                                                        }
                                                    }
                                                }
                                            })
                                        })
                                    }
                                }
                            })
                        }
                    });
                });*/
                res.json(campeonatos);
            }
        }
    });
};
exports.criar = function(req, res) {
    var new_campeonato = new Campeonato(req.body);
    new_campeonato.save(function(err, campeonato) {
        if (err) {
            res.send(err);
        } else {
            res.json(campeonato);
        }
    });
};
exports.ler = function(req, res) {
    Campeonato.findById(req.params.campeonatoId, function(err, campeonato) {
        if (err) {
            res.send(err);
        } else {
            Clube.findById(campeonato.idClube, function(err, clube) {
                if (err) {
                    res.send(err);
                } else {
                    campeonato.set('clube', clube, { strict: false });
                    res.json(campeonato);
                }
            })
        }
    });
};
exports.editar = function(req, res) {
    Campeonato.findOneAndUpdate({_id: req.params.campeonatoId}, req.body, {new: true}, function(err, campeonato) {
        if (err) {
            res.send(err);
        } else {
            res.json(campeonato);
        }
    });
};
exports.excluir = function(req, res) {
    Campeonato.deleteOne({ _id: req.params.campeonatoId }, function(err, campeonato) {
        if (err) {
            res.send(err);
        } else {
            res.json({ mensagem: 'Campeonato excluído com sucesso' });
        }
    });
};
exports.listarEventos = function(req, res) {
    var query = { idCampeonato:  req.params.campeonatoId }
    CampeonatoEvento.find(query, function(err, eventos) {
        if (err) {
            res.send(err);
        } else {
            res.json(eventos);
        }
    });
}
exports.vincularEvento = function(req, res) {
    var new_campeonato_evento = new CampeonatoEvento(req.body);
    new_campeonato_evento.save(function(err, campeonato_evento) {
        if (err) {
            res.send(err);
        } else {
            res.json(campeonato_evento);
        }
    });
};
exports.desvincularEvento = function(req, res) {
    var query = { 'idCampeonato': req.body.idCampeonato, 'idEvento': req.body.idEvento };
    CampeonatoEvento.deleteOne(query, function(err, evento) {
        if (err) {
            res.send(err);
        } else {
            res.json({ mensagem: 'Evento não faz mais parte deste campeonato' });
        }
    });
};