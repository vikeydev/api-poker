'use strict';
var mongoose = require('mongoose'),
Clube = mongoose.model('Clube'),
Integrante = mongoose.model('Integrante'),
Jogador = mongoose.model('Jogador');
exports.listar = function(req, res) {
    Integrante.find(req.query, function(err, integrantes) {
        if (err) {
            res.send(err);
        } else {
            var qtdIntegrantes = integrantes.length;
            var currentIntegranteIdx = 0;
            var integrantesCompletos = [];
            
            if(qtdIntegrantes <= 0) {
                res.json([]);
            } else {
                // percorrer integrantes
                integrantes.forEach(function(integrante) {
                    // buscar Clube
                    Clube.findById(integrante.idClube, function(err, clube) {
                        if (err) {
                            res.send(err);
                        } else {
                            integrante.set('clube', clube, { strict: false });
                            
                            // buscar Jogador
                            Jogador.findById(integrante.idJogador, function(err, jogador) {
                                if (err) {
                                    res.send(err);
                                } else {
                                    integrante.set('jogador', jogador, { strict: false });

                                    integrantesCompletos.push(integrante);
                                    ++currentIntegranteIdx;
                                    if (currentIntegranteIdx == qtdIntegrantes) {
                                        res.json(integrantesCompletos);
                                    }
                                }
                            });
                        }
                    });
                });
            }
        }
    });
};
exports.criar = function(req, res) {
    var new_integrante = new Integrante(req.body);
    new_integrante.save(function(err, integrante) {
        if (err) {
            res.send(err);
        } else {
            res.json(integrante);
        }
    });
};
exports.ler = function(req, res) {
    Integrante.findById(req.params.integranteId, function(err, integrante) {
        if (err) {
            res.send(err);
        } else {
            res.json(integrante);
        }
    });
};
exports.editar = function(req, res) {
    Integrante.findOneAndUpdate({_id: req.params.integranteId}, req.body, {new: true}, function(err, integrante) {
        if (err) {
            res.send(err);
        } else {
            res.json(integrante);
        }
    });
};
exports.excluir = function(req, res) {
    Integrante.deleteOne({ _id: req.params.integranteId }, function(err, integrante) {
        if (err) {
            res.send(err);
        } else {
            res.json({ mensagem: 'Jogador não faz mais parte deste Clube' });
        }
    });
};