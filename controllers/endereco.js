'use strict';
var mongoose = require('mongoose'),
Endereco = mongoose.model('Endereco');
exports.listar = function(req, res) {
    Endereco.find(req.query, function(err, endereco) {
        if (err) {
            res.send(err);
        } else {
            res.json(endereco);
        }
    });
};
exports.criar = function(req, res) {
    var new_endereco = new Endereco(req.body);
    new_endereco.save(function(err, endereco) {
        if (err) {
            res.send(err);
        } else {
            res.json(endereco);
        }
    });
};
exports.ler = function(req, res) {
    Endereco.findById(req.params.enderecoId, function(err, endereco) {
        if (err) {
            res.send(err);
        } else {
            res.json(endereco);
        }
    });
};
exports.editar = function(req, res) {
    Endereco.findOneAndUpdate({_id: req.params.enderecoId}, req.body, {new: true}, function(err, endereco) {
        if (err) {
            res.send(err);
        } else {
            res.json(endereco);
        }
    });
};
exports.excluir = function(req, res) {
    Endereco.deleteOne({ _id: req.params.enderecoId }, function(err, endereco) {
        if (err) {
            res.send(err);
        } else {
            res.json({ mensagem: 'Endereco excluído com sucesso' });
        }
    });
};