'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var enderecoSchema = new Schema({
    apelido: {
        type: String
    },
    pais: {
        type: String
    },
    cep: {
        type: String
    },
    cidade: {
        type: String
    },
    zona: {
        type: String
    },
    bairro: {
        type: String
    },
    logradouro: {
        type: String
    },
    prefixo: {
        type: String
    },
    numero: {
        type: Number
    },
    complemento: {
        type: String
    },
    linkGoogle: {
        type: String
    },
    comentario: {
        type: String
    },
    dataCriacao: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Endereco', enderecoSchema);