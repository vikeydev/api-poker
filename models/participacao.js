'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var participacaoSchema = new Schema({
    idJogador: {
        type: String
    },
    idEvento: {
        type: String
    },
    qtdBuyIn: {
        type: Number,
        default: 0
    },
    qtdReBuy: {
        type: Number,
        default: 0
    },
    qtdAddOn: {
        type: Number,
        default: 0
    },
    investimento: {
        type: Number,
        default: 0
    },
    faturamento: {
        type: Number,
        default: 0
    },
    saldo: {
        type: Number,
        default: 0
    },
    roi: {
        type: Number,
        default: 0
    },
    dataConfirmacao: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Participacao', participacaoSchema);