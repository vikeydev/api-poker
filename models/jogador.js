'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var jogadorSchema = new Schema({
    nome: {
        type: String
    },
    login: {
        type: String
    },
    email: {
        type: String
    },
    dataCriacao: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Jogador', jogadorSchema);