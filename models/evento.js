'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var eventoSchema = new Schema({
    idClube: {
        type: String
    },
    idEndereco: {
        type: String
    },
    nome: {
        type: String
    },
    duracao: { // horas
        type: Number
    },
    dataProgramaInicio: {
        type: Date
    },
    dataProgramadaIntervalo: {
        type: Date,
        default: null
    },
    intervalo: { // minutos
        type: Number,
        default: 0
    },
    dataProgramadaFim: {
        type: Date
    },
    formato: {
        type: String // { "CashGame", "Torneio" }
    },
    moeda: {
        type: String
    },
    buyIn: {
        type: Number
    },
    reBuy: {
        type: Number
    },
    addOn: {
        type: Number,
        default: 0
    },
    smallBlind: {
        type: Number
    },
    bigBlind: {
        type: Number
    },
    ante: {
        type: Number,
        default: 0
    },
    dataInicio: {
        type: Date,
        default: null
    },
    dataIntervalo: {
        type: Date,
        default: null
    },
    dataFim: {
        type: Date,
        default: null
    },
    qtdParticipante: {
        type: Number,
        default: 0
    },
    investimento: {
        type: Number,
        default: 0
    },
    qtdBuyIn: {
        type: Number,
        default: 0
    },
    qtdReBuy: {
        type: Number,
        default: 0
    },
    qtdAddOn: {
        type: Number,
        default: 0
    },
    status: {
        type: String, // { 'Programado', 'Cancelado', 'Iniciado', 'Encerrado' }
        default: "Programado"
    },
    dataCriacao: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Evento', eventoSchema);