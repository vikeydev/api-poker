'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var rankingSchema = new Schema({
    idClube: {
        type: String,
        default: ''
    },
    idCampeonato: {
        type: String,
        default: ''
    },
    idJogador: {
        type: String
    },
    investimento: {
        type: Number,
        default: 0
    },
    faturamento: {
        type: Number,
        default: 0
    },
    saldo: {
        type: Number,
        default: 0
    },
    roi: {
        type: Number,
        default: 0
    },
    frequencia: {
        type: Number,
        default: 0
    },
    naDisputa: {
        type: String,
        default: true
    },
    melhorFaturamento: {
        type: Number,
        default: 0
    },
    posicaoAtual: {
        type: Number,
        default: 0
    },
    posicaoAnterior: {
        type: Number,
        default: 0
    },
    qtdEvento: {
        type: Number,
        default: 0
    },
    dataAtualizacao: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Ranking', rankingSchema);