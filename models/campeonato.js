'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var campeonatoSchema = new Schema({
    idClube: {
        type: String
    },
    nome: {
        type: String
    },
    custoPorEvento: {
        type: Number,
        default: 0
    },
    frequenciaMinima: {
        type: Number,
        default: 0
    },
    dataInicio: {
        type: Date
    },
    dataFim: {
        type: Date
    },
    dataPremiacao: {
        type: Date
    },
    valorPremiacao: {
        type: Number,
        default: 0
    },
    investimento: {
        type: Number,
        default: 0
    },
    melhorFaturamento: {
        type: Number,
        default: 0 
    },
    idJogadorMelhorFaturamento: {
        type: String,
        default: ""
    },
    qtdEvento: {
        type: Number,
        default: 0 
    },
    dataCriacao: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Campeonato', campeonatoSchema);