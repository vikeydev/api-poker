'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var clubeSchema = new Schema({
    nome: {
        type: String
    },
    investimento: {
        type: Number,
        default: 0
    },
    melhorFaturamento: {
        type: Number,
        default: 0 
    },
    idJogadorMelhorFaturamento: {
        type: String,
        default: ""
    },
    qtdEvento: {
        type: Number,
        default: 0 
    },
    dataCriacao: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Clube', clubeSchema);