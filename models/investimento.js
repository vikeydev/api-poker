'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var investimentoSchema = new Schema({
    idJogador: {
        type: String
    },
    idEvento: {
        type: String
    },
    tipo: {
        type: String // { "buyIn", "reBuy", "addOn" }
    },
    valor: {
        type: Number
    },
    dataCriacao: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Investimento', investimentoSchema);