'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var integranteSchema = new Schema({
    idClube: {
        type: String
    },
    idJogador: {
        type: String
    },
    dataCriacao: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Integrante', integranteSchema);