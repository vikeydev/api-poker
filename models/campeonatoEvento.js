'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var campeonatoEventoSchema = new Schema({
    idClube: {
        type: String
    },
    idCampeonato: {
        type: String
    },
    idEvento: {
        type: String
    },
    dataCriacao: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('CampeonatoEvento', campeonatoEventoSchema);