var express = require('express'),
app = express(),
port = process.env.PORT || 3000,
mongoose = require('mongoose'),
Campeonato = require('./models/campeonato'),
CampeonatoEvento = require('./models/campeonatoEvento'),
Clube = require('./models/clube'),
Endereco = require('./models/endereco'),
Evento = require('./models/evento'),
Integrante = require('./models/integrante'),
Investimento = require('./models/investimento'),
Jogador = require('./models/jogador'),
Participacao = require('./models/participacao'),
Ranking = require('./models/ranking'),
bodyParser = require('body-parser');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/pokerdb', { useNewUrlParser: true });
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var routes = require('./roteamento');
routes(app);
app.listen(port);
console.log('Poker RESTful API server started on: ' + port);