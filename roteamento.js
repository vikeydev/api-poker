'use strict';
module.exports = function(app) {
var campeonato = require('./controllers/campeonato');
var clube = require('./controllers/clube');
var endereco = require('./controllers/endereco');
var evento = require('./controllers/evento');
var integrante = require('./controllers/integrante');
var investimento = require('./controllers/investimento');
var jogador = require('./controllers/jogador');
var participacao = require('./controllers/participacao');
var ranking = require('./controllers/ranking');

// campeonato Routes
app.route('/campeonato')
    .get(campeonato.listar)
    .post(campeonato.criar);
app.route('/campeonato/:campeonatoId')
    .get(campeonato.ler)
    .put(campeonato.editar)
    .delete(campeonato.excluir);
app.route('/campeonato/listarEventos/:campeonatoId')
    .get(campeonato.listarEventos);
app.route('/campeonato/vincularEvento')
    .post(campeonato.vincularEvento);
app.route('/campeonato/desvincularEvento')
    .post(campeonato.desvincularEvento);

// clube Routes
app.route('/clube')
    .get(clube.listar)
    .post(clube.criar);
app.route('/clube/:clubeId')
    .get(clube.ler)
    .put(clube.editar)
    .delete(clube.excluir);

// evento Routes
app.route('/evento')
    .get(evento.listar)
    .post(evento.criar);
app.route('/evento/:eventoId')
    .get(evento.ler)
    .put(evento.editar)
    .delete(evento.excluir);
app.route('/evento/listarCampeonatos/:eventoId')
    .get(evento.listarCampeonatos)
app.route('/evento/finalizar/:eventoId')
    .get(evento.finalizar);

// endereco Routes
app.route('/endereco')
    .get(endereco.listar)
    .post(endereco.criar);
app.route('/endereco/:enderecoId')
    .get(endereco.ler)
    .put(endereco.editar)
    .delete(endereco.excluir);

// integrante Routes
app.route('/integrante')
    .get(integrante.listar)
    .post(integrante.criar);
app.route('/integrante/:integranteId')
    .get(integrante.ler)
    .delete(integrante.excluir);

// investimento Routes
app.route('/investimento')
    .get(investimento.listar)
    .post(investimento.criar);
app.route('/investimento/:investimentoId')
    .get(investimento.ler)
    .put(investimento.editar)
    .delete(investimento.excluir);
app.route('/investimento/excluirUltimo')
    .post(investimento.excluirUltimoInvestimentoJogadorEvento);

// jogador Routes
app.route('/jogador')
    .get(jogador.listar)
    .post(jogador.criar);
app.route('/jogador/:jogadorId')
    .get(jogador.ler)
    .put(jogador.editar)
    .delete(jogador.excluir);

// participacao Routes
app.route('/participacao')
    .get(participacao.listar)
    .post(participacao.criar);
app.route('/participacao/:participacaoId')
    .get(participacao.ler)
    .put(participacao.editar)
    .delete(participacao.excluir);
app.route('/participacao/finalizar')
    .post(participacao.finalizar);

// ranking Routes
app.route('/ranking')
    .get(ranking.listar)
    .post(ranking.criar);
app.route('/ranking/:rankingId')
    .get(ranking.ler)
    .put(ranking.editar)
    .delete(ranking.excluir);
};